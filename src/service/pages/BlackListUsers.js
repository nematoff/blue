import http from '../../plugins/http'

class BlackListUsers {
  async getBlackListUsers(page) {
    return await http.get('black-list-users', {
      params: page
    })
  }
  async create(form) {
    return await http.post('black-list-users', form)
  }

  async update(id, form) {
    return await http.put(`black-list-users/${id}`, form)
  }
  async getOne(id) {
    return await http.get(`black-list-users/${id}`)
  }
  deleteItem(id) {
    return http.delete(`black-list-users/${id}`)
  }

  importExel(file) {
    return http.post(`black-list-users/import`, file )
  }

  async downloadPdf(id) {
    return await http.get('reports/pdf-export/' + id,{params: {}, responseType: 'blob'} )
  }

  async downloadScoringPdf(form) {
    return await http.post('search/pdf-export', form,{params: {}, responseType: 'blob'} )
  }
}

export default new BlackListUsers()
