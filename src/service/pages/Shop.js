import http from '../../plugins/http'

class Shop {
  async getShop(params) {
    return await http.get('shops', {
      params: params
    })
  }

  async getOneShop(id) {
    return await http.get(`shops/${id}`)
  }

  async update(id, formData) {
    return await http.post(`shops/${id}`, formData)
  }

  async create(formData) {
    return await http.post(`shops`, formData)
  }

  deleteItem(val) {
    return http.delete(`shops/${val.id}`)
  }

  async getViewItem(id) {
    return await http.get(`shops/${id}`)
  }

  async getList() {
    return await http.get(`shops/list`)
  }

  shopChangeUserPassword(form) {
    return http.post('shop-cabinet/set-new-password', form)
  }

  adminChangeUserPassword(form) {
    return http.post('shops/set-new-password', form)
  }

  toUpBalance(id, form) {
    return http.put(`shops/top-up-balance/${id}`, form)
  }

  async changeStatus(id) {
    return await http.post(`shops/change-status/${id}`)
  }
}

export default new Shop()
