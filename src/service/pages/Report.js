import http from '../../plugins/http'

class Report {
  async getReport(params) {
    let config = {
      params: params
    }
    if (params.excel)
      config.responseType = 'blob'
    return await http.get('shop-cabinet/search-reports', config)
  }

  async getReportAdmin(params) {
    let config = {
      params: params
    }
    if (params.excel)
      config.responseType = 'blob'
    return await http.get('/reports/search-report', config)
  }
}

export default new Report()
