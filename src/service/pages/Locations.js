import http from '../../plugins/http'

class Locations {
  async getLocations({ page }) {
    return await http.get('locations', {
      params: {
        page: page
      }
    })
  }

  async getLocationsList() {
    return await http.get('locations/parents')
  }

  async getListForShopCabinet() {
    return await http.get('location/parents')
  }


  async create(form) {
    return await http.post('locations', form)
  }

  async update(id, form) {
    return await http.put(`locations/${id}`, form)
  }
  async deleteItem(id) {
    return await http.delete(`locations/${id}`)
  }

}

export default new Locations()
