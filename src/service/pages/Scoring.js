import http from '../../plugins/http'

class Scoring {
    async search(filter) {
    return await http.get('search', {
      params: {
        ...filter
      }
    })
  }
}

export default new Scoring()
