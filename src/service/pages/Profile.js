import http from '../../plugins/http'

class PriceApi {
  async saveProfileDataShop(formData) {
    return await http.post('shop-cabinet/update', formData)
  }
}

export default new PriceApi()
