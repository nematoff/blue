import http from '../../plugins/http'

class ShopCabinet {
  async getShopCabinet({ page }) {
    return await http.get('shop-cabinet/blacklist-users', {
      params: {
        page: page
      }
    })
  }

  async getOneShopCabinet(id) {
    return await http.get(`shop-cabinet/blacklist-users/${id}`)
  }

  async create(form) {
    return await http.post('/shop-cabinet/blacklist-users', form)
  }

  async update(id, form) {
    return await http.put(`/shop-cabinet/blacklist-users/${id}`, {shops: form})
  }

  deleteItem(val) {
    return http.delete(`shop-cabinet/blacklist-users/${val.id}` )
  }
  importExel(file) {
    return http.post(`shop-cabinet/blacklist-users/import`, file )
  }
}

export default new ShopCabinet()
