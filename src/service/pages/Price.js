import http from '../../plugins/http'

class PriceApi {
  async getPriceList() {
    return await http.get('price-list')
  }

  async getPriceListType() {
    return await http.get('price-list/type-list')
  }

  async createOrUpdate(formData) {
    return await http.post('price-list/create-or-update', formData)
  }
}

export default new PriceApi()
