import http from '../../plugins/http'

class Transactions {
  async getShopTransactions(params) {
    let config = {
      params: params
    };
    if(params.excel)
      config.responseType = 'blob';
    return await http.get('shop-cabinet/transactions', config)
  }
  async getAdminTransactions(params) {
    let config = {
      params: params
    };
    if(params.excel)
      config.responseType = 'blob';

    return await http.get('transactions', config)
  }
  async cancelPayment(id) {
    return await http.post(`/transactions/reverse/${id}`)
  }
}

export default new Transactions()
