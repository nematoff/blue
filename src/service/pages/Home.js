import http from '../../plugins/http'

class Home {
  async getHome(filter) {
    return await http.get('home/search-blacklist-users', {
      params: {
        ...filter
      }
    })
  }
  async getType() {
    return await http.get('price-list/type-list')
  }

  async   viewHome(id, params) {
    return await http.get(`home/search-blacklist-users/${id}`, {
      params: params
    })
  }
  async viewExelDownload(id) {
    return await http.get(`reports/pdf-export/${id}`, )
  }
}

export default new Home()
