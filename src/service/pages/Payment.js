import http from '../../plugins/http'

class Payment {
  async getPaymentType() {
    return await http.get('payment/types')
  }
  async getPaymentAddBalance(form) {
    return await http.post('payment/add-balance', form)
  }
  async confirmCode(form) {
    return await http.post('payment/confirm-balance', form)
  }
  async buyTariff(form) {
    return await http.post('shop-cabinet/buy-tariff', form)
  }
  async getPriceWithBonus() {
    return await http.get('price-with-bonus')
  }
}

export default new Payment()
