import http from '../../plugins/http'

class Auth {
  async fetchLogin(form) {
    return await http.post('auth/login', form)
  }

  fetchMe() {
    return http.post('auth/me')
  }
}

export default new Auth()
