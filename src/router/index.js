import { createRouter, createWebHistory } from 'vue-router'
import SideMenu from '../layouts/side-menu/Main.vue'
import SimpleMenu from '../layouts/simple-menu/Main.vue'
import TopMenu from '../layouts/top-menu/Main.vue'
import DashboardOverview1 from '../views/dashboard-overview-1/Main.vue'
import Login from '../views/login/Main.vue'
import ErrorPage from '../views/error-page/Main.vue'
import Locations from '../views/pages/locations/index.vue'
import ShopCabinet from '../views/pages/shop-cabinet/index.vue'
import ShopCabinetForm from '../views/pages/shop-cabinet/form'
import Shop from '../views/pages/shop/index.vue'
import Scoring from '../views/pages/scoring/index.vue'
import ShopForm from '../views/pages/shop/form.vue'
import Home from '../views/pages/home/index.vue'
import HomeView from '../views/pages/home/view.vue'
import PriceList from '../views/pages/price-list/index.vue'
import BlackListUser from '../views/pages/black-list-user/index.vue'
import BlackListUserForm from '../views/pages/black-list-user/form.vue'
import Transactions from '../views/pages/transactions/index.vue'
import Report from '../views/pages/report/index.vue'
import Profile from '../views/pages/profile/index.vue'
import ScoringView from '../views/pages/scoring/view.vue'
import { service } from '../utils/LocalStorageService'

const routes = [
  {
    path: '/',
    component: SideMenu,
    children: [
      {
        path: '/dashboard',
        name: 'side-menu-dashboard',
        component: DashboardOverview1
      },
      {
        path: '/scoring',
        name: 'side-menu-scoring',
        component: Scoring
      },
      {
        path: 'locations',
        name: 'side-menu-locations',
        component: Locations
      },
      {
        path: 'shop-cabinet',
        name: 'side-menu-shop-cabinet',
        component: ShopCabinet
      },
      {
        path: '/shop-cabinet-form/:id?',
        name: 'side-menu-shop-cabinet-form',
        meta: {
          back: true
        },
        component: ShopCabinetForm
      },
      {
        path: 'shop',
        name: 'side-menu-shop',
        component: Shop
      },
      {
        path: '/shop-form/:id?',
        name: 'side-menu-shop-form',
        meta: {
          back: true
        },
        component: ShopForm
      },

      /**  home  **/

      {
        path: 'home',
        name: 'side-menu-home',
        component: Home
      },
      {
        path: 'price-list',
        name: 'side-menu-price-list',
        component: PriceList
      },

      /**  black-list-user  **/

      {
        path: 'black-list-users',
        name: 'side-menu-black-list-users',
        component: BlackListUser
      },
      {
        path: 'black-list-users-form/:id?',
        name: 'side-menu-black-list-users-form',
        meta: {
          back: true
        },
        component: BlackListUserForm
      },


      {
        path: 'transactions',
        name: 'side-menu-transactions',
        component: Transactions
      },
      {
        path: 'report',
        name: 'side-menu-report',
        component: Report
      },
      {
        path: 'profile',
        name: 'side-menu-profile',
        component: Profile
      }
    ]
  },
  {
    path: '/simple-menu',
    component: SimpleMenu,
    children: [
      {
        path: '/dashboard',
        name: 'simple-menu-dashboard',
        component: DashboardOverview1
      },
      {
        path: 'locations',
        name: 'simple-menu-locations',
        component: Locations
      },
      {
        path: 'shop-cabinet',
        name: 'simple-menu-shop-cabinet',
        component: ShopCabinet
      },
      {
        path: 'shop',
        name: 'simple-menu-shop',
        component: Shop
      },
      {
        path: 'home',
        name: 'simple-home',
        component: Home
      },
      {
        path: 'price-list',
        name: 'simple-home-price-list',
        component: PriceList
      },

      {
        path: 'transactions',
        name: 'simple-menu-transactions',
        component: Transactions
      },
      {
        path: 'black-list-users',
        name: 'simple-menu-black-list-users',
        component: BlackListUser
      }
    ]
  },
  {
    path: '/top-menu',
    component: TopMenu,
    children: [
      {
        path: '/dashboard',
        name: 'top-menu-dashboard',
        component: DashboardOverview1
      },
      {
        path: 'locations',
        name: 'top-menu-locations',
        component: Locations
      },
      {
        path: 'shop-cabinet',
        name: 'top-menu-shop-cabinet',
        component: ShopCabinet
      },
      {
        path: 'shop',
        name: 'top-menu-shop',
        component: Shop
      },

      {
        path: 'transactions',
        name: 'top-menu-transactions',
        component: Transactions
      },
      {
        path: 'home',
        name: 'top-menu-home',
        component: Home
      },
      {
        path: 'price-list',
        name: 'top-menu-price-list',
        component: PriceList
      },
      {
        path: 'black-list-users',
        name: 'top-menu-black-list-users',
        component: BlackListUser
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      isPublic: true,
      logout: true
    },
    component: Login
  },

  // home-view route name ga qarab app.vue da style da ozgarish bo'lgan
  // o'zgartirishda tekshirish kerak

  {
    path: '/home-view/:id/:user?',
    name: 'home-view',
    component: HomeView
  },

  {
    path: '/scoring-view',
    name: 'scoring-view',
    component: ScoringView
  },

  // eslatma tepada

  {
    path: '/error-page',
    name: 'error-page',
    component: ErrorPage
  },
  {
    path: '/:pathMatch(.*)*',
    component: ErrorPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || {
      left: 0,
      top: 0
    }
  }
})

router.beforeEach((to, from, next) => {
  const token = !!service.getToken()
  const logout = to.matched.some(record => record.meta.logout)
  const isPublic = to.matched.some(record => record.meta.isPublic)
  if (token && logout) {
    return next({ name: 'authority' })
  }

  if (!token && !isPublic) {
    return next({ name: 'login' })
  }

  next()
})

router.options.history.location.includes('home-view') ? cash('html').addClass('background-for-view-home') : cash('html').removeClass('background-for-view-home')

export default router
