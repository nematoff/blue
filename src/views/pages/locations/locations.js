import Locations from '../../../service/pages/Locations'
import { ref } from 'vue'

const locationsApi = {
  async getLocations() {
    let locationsList = ref([])
    try {
      const { data: { data } } = await Locations.getLocations()
      locationsList = data
      return locationsList
    } catch (err) {
      console.log(err)
    }
  }
}

export default locationsApi
