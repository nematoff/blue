import { service } from '../../../utils/LocalStorageService'
import Auth from '../../../service/auth/Auth'

const refreshBalanceMixin = {
  data() {
    return {
      userInfo: {},
    }
  },
  methods: {
    getServiceUserInfo() {
      if (service.getUserInfo()) {
        this.userInfo = service.getUserInfo()
      }
    },
    async refreshMe() {
      if (this.userType === 'shop') {
        try {
          const { data: { data } } = await Auth.fetchMe()
          service.setUserInfo(data.shop)
          this.$store.dispatch('getUserInfo', data.shop)
          this.userInfo = data.shop
          debugger
          setTimeout(() => {
            this.getServiceUserInfo()
          }, 400)
        } catch (err) {
          this.$setErrorFromServer({ error: err })
        }
      }
    },
  },
  computed: {
    info() {
      return this.$store.state.userInfo
    }
  }
}


export default refreshBalanceMixin
