import Auth from '../../service/auth/Auth'

const loginApi = {
  async login(form) {
    return await Auth.fetchLogin(form)
  },
  async auth() {
    return await Auth.fetchMe()
  }
}

export default loginApi
