export default {
  oz: {
    'required': '@:{_field_} майдони тўлдирилиши шарт',
    'is-login-or-email': 'Нотўғри логин ёки почта',
    'email': 'Нотўғри почта',
    confirmed: 'Пароллар мос келмаяпти',
    'phone-number': 'Нотўғри телефон рақам',
    'max': '{_field_} майдони {_max_} символдан коп булмалиги керак'
  },
  ru: {
    'required': 'Поле @:{_field_} обязательно для заполнения',
    'is-login-or-email': 'Недействительный логин или Email',
    'email': 'Недействительный Email',
    confirmed: 'Пароли не совпадает',
    'phone-number': 'Недействительный телефон номер',
    'max': 'Поле {_field_} не может быть более {_max_} символов'
  },
  uz: {
    'required': '@:{_field_} maydoni to\'ldirilishi shart',
    'email': 'Noto\'g\'ri pochta',
    'is-login-or-email': 'Noto\'g\'ri login yoki pochta',
    confirmed: 'Parollar mos kelmayapti',
    'phone-number': 'Noto\'g\'ri telefon raqam',
    'max': '{_field_} maydoni {_max_} simboldan ko`p bo`lmasligi kerak'
  }
}
