import { configure, defineRule } from 'vee-validate'
import {email, size, required, max, min, numeric, min_value} from '@vee-validate/rules'
import { $t } from './i18n'

defineRule('required', required)
defineRule('max', max)
defineRule('size', size)
defineRule('email', email)
defineRule('min', min)
defineRule('numeric', numeric)
defineRule('min_value', min_value)


export default {
  install(app) {
    configure({
      generateMessage: context => {
        const params = {'_field_': context.field}
        params[`_${context.rule.name}_`] = context.rule.params.join(', ',)
        return $t(`$validation.${context.rule.name}`, params);
      },
    });

  }
}
