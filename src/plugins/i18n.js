import {createI18n} from 'vue-i18n/index'
import messages from './messages'

const i18n = createI18n({
  locale: 'ru',
  messages: messages,
  silentTranslationWarn: true
});

export const $t = i18n.global.t

window['$t'] = i18n.global

export default i18n;
