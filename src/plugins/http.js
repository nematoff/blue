import axios from 'axios'
import router from '../router'
import { service } from '../utils/LocalStorageService'

const baseUrl = 'https://api.blueinfo.uz/api/v1/admin'
// const baseUrl = 'http://blue.loc/api/v1/admin'  //locale
const http = axios.create(({
  baseURL: baseUrl
}))

http.interceptors.request.use(config => {
  const token = service.getToken()
  const lang = service.getLanguage()
  if(lang) {
    config.headers.Language = lang.value
  }
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
})

http.interceptors.response.use(res => res, error => {
  if (error.response && (error.response.status === 401 || error.response.status === 403)) {
    service.removeToken()
    if (router.options.history.location !== '/login') {
      router.push('/login')
    }
  }
  return Promise.reject(error);
})

export default http
