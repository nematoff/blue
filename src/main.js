import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import globalMixin from './global-mixin'
import globalComponents from './global-components'
import utils from './utils'
import i18n from './plugins/i18n'
import './libs'
import VeeValidate from './plugins/vee-validate'
import 'element-plus/dist/index.css'
import { ElLoading } from 'element-plus'

// SASS Theme
import './assets/sass/app.scss'
import TokenService, { service } from './utils/LocalStorageService'
import './assets/tailwind.css'


(async function() {
  const token = service.getToken()
  if(token) {
    try {
      await store.dispatch('getUser')
    } catch (e) {
    }
  }
  const app = createApp(App)
    .use(store)
    .use(router)

  globalComponents(app)
  app.config.globalProperties.$parseDate = (date) => {
    return new Date(date).toLocaleDateString()
  }
  utils(app)
  app.mixin(globalMixin)
  app.use(TokenService)
  app.use(i18n)
  app.use(ElLoading)
  app.use(VeeValidate)
  app.mount('#app')
})()


