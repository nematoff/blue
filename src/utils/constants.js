import i18n from '../plugins/i18n'

export const statusList = [
  {
    value: 10,
    text: 'active'
  },
  {
    value: 0,
    text: 'notActive'
  }
]


export const STATUS = {
  10: 'active',
  0: 'notActive'
}
