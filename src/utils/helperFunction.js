const ID = (id) => {
  return () => id++
}

export const UID = ID(1)


export const getValueByKey = (obj1, obj2) => {
  let resultObj = {}
  Object.keys(obj1).forEach(p => {
    if(obj2[p]) {
      resultObj[p] = obj2[p];
    } else {
      resultObj[p] = obj1[p];
    }
  })
  return resultObj;
}

export const getValueByHaveKey = (obj1, obj2) => {
  let resultObj = {}
  Object.keys(obj1).forEach(p => {
    if(obj2[p]) {
      resultObj[p] = obj2[p];
    }
  })
  return resultObj;
}


export const debounce = function (func, delay) {
  let time = null;
  return function () {
    if (time !== null) {
      clearTimeout(time);
    }
    time = setTimeout(() => {
      func.apply(this);
    }, delay);
  };
};

export const phoneFormat = (phone) => {
  return phone !== '' ? phone
    // .toString().replace(/(\d{3})(\d{2})(\d{3})(\d{2})(\d{2})/, "+$1 $2 $3-$2-$2") : '';
    .toString()
    .replace(/\D+/g, '')
    .replace(/(\d{3})(\d{2})(\d{3})/, '+$1$2 $3-') : '';
}


export const clearingNumbers = (val) => {
  if(val) {
    return  val.replace(/\D/g, "")
  } else {
    return val
  }
}

export const downloadBlobFile = (blob, fileName = 'file') => {
  const a = document.createElement("a");
  document.body.appendChild(a);
  a.style.display = "none";
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = fileName;
  a.click();
  window.URL.revokeObjectURL(url);
  a.remove()
}
