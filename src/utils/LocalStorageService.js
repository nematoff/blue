const TOKEN_KEY = 'Authorization'
const USER_KEY = 'user'
const USER_INFO_KEY = 'user_info'

const LocalStorageService = {
  setToken(token) {
    localStorage.setItem(TOKEN_KEY, token)
  },
  getToken() {
    return localStorage.getItem(TOKEN_KEY)
  },
  removeToken() {
    localStorage.removeItem(TOKEN_KEY)
    localStorage.clear()
  },
  setUserType(type) {
    localStorage.setItem(USER_KEY, type)
  },
  getUserType() {
    return localStorage.getItem(USER_KEY)
  },
  setUserInfo(info) {
    localStorage.setItem(USER_INFO_KEY, JSON.stringify(info))
  },
  getUserInfo() {
    return JSON.parse(localStorage.getItem(USER_INFO_KEY))
  },
  getLanguage() {
    return JSON.parse(localStorage.getItem('lang'))
  }
}
const install = app => {
  app.config.globalProperties.$LocalStorageService = () => {
    return LocalStorageService
  }
}
export { install as default, LocalStorageService as service }
