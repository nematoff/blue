import Toasify from 'toastify-js'
import Auth from '../service/auth/Auth'
import { service } from '../utils/LocalStorageService'

let lastErrorMessage = null

const globalMixin = {
  computed: {
    userType() {
      return service.getUserType()
    }
  },
  methods: {
    $showLoading() {
      this.$store.dispatch('setLoading', true)
    },
    $clearLoading() {
      this.$store.dispatch('setLoading', false)
    },
    $successNotify({ text }) {
      this.$store.dispatch('showNotify', {
        text: text,
        type: 'success'
      })
      this.$nextTick(() => {
        Toasify({
          node: cash('#success-notification-content')
            .clone()
            .removeClass('hidden')[0],
          duration: 3000,
          newWindow: true,
          close: true,
          gravity: 'top',
          position: 'right',
          stopOnFocus: true
        }).showToast()
      }).catch(err => err)
    },
    $setErrorFromServer({ error }) {
      const text = (error && error.response && error.response.data && error.response.data.message) || error.message
      if (text && lastErrorMessage !== text) {
        this.$store.dispatch('showNotify', {
          text: text,
          type: 'error'
        })
        this.$nextTick(() => {
          Toasify({
            node: cash('#success-notification-content')
              .clone()
              .removeClass('hidden')[0],
            duration: 3000,
            newWindow: true,
            close: true,
            gravity: 'top',
            position: 'right',
            stopOnFocus: true
          }).showToast()
        }).catch(err => err)
        lastErrorMessage = text
        setTimeout(() => {
          lastErrorMessage = null
        }, 2000)
      }
    },
    $setFormServer({ error, obs }) {
      if (error && error.response && error.response.data && error.response.data.message && obs) {
        obs.setErrors(error.response.data.errors)
      }
    }
  }
}

export default globalMixin
