const state = () => {
  return {
    menu: [
      {
        icon: 'BoxIcon',
        pageName: 'top-menu-menu-layout',
        title: 'Menu Layout',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Side Menu',
            ignore: true
          },
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Simple Menu',
            ignore: true
          },
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Top Menu',
            ignore: true
          }
        ]
      },
      {
        icon: 'ActivityIcon',
        pageName: 'top-menu-apps',
        title: 'Apps',
        subMenu: [
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-example',
            title: 'Example'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-shop-cabinet',
            title: 'Shop cabinet'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-shop',
            title: 'Shop'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-locations',
            title: 'Locations'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-home',
            title: 'Home'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-price-list',
            title: 'Price'
          },
          {
            icon: 'InboxIcon',
            pageName: 'top-menu-black-list-users',
            title: 'Black list user'
          }
        ]
      },
      {
        icon: 'LayoutIcon',
        pageName: 'top-menu-layout',
        title: 'Pages',
        subMenu: [
          {
            icon: '',
            pageName: 'login',
            title: 'Login'
          },
          {
            icon: '',
            pageName: 'register',
            title: 'Register'
          },
          {
            icon: '',
            pageName: 'error-page',
            title: 'Error Page'
          },
          {
            icon: '',
            pageName: 'top-menu-update-profile',
            title: 'Update profile'
          },
          {
            icon: '',
            pageName: 'top-menu-change-password',
            title: 'Change Password'
          }
        ]
      }
    ]
  }
}

// getters
const getters = {
  menu: state => state.menu
}

// actions
const actions = {}

// mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
