import { createStore } from 'vuex'
import main from './main'
import sideMenu from './side-menu'
import simpleMenu from './simple-menu'
import topMenu from './top-menu'
import Auth from '../service/auth/Auth'

const store = createStore({
  state: {
    notify: {},
    loading: false,
    userInfo: {},
    username: null
  },
  getters: {
    notify: state => state.notify,
    loading: state => state.loading,
    userInfo: state => state.userInfo,
    username: state => state.username
  },

  mutations: {
    writeNotifyText: (state, payload) => {
      state.notify = payload
    },
    loading: (state, payload) => {
      state.loading = payload
    },
    userInfo: (state, payload) => {
      state.userInfo = payload.shop
      state.username = payload.username
    }
  },

  actions: {
    showNotify({ commit }, payload) {
      commit('writeNotifyText', payload)
    },
    setLoading({commit}, payload) {
      commit('loading', payload)
    },
    getUserInfo({commit}, payload) {
      commit('userInfo', payload)
    },
    async getUser({commit}) {
      try {
        const {data: {data}} = await Auth.fetchMe()
        commit('userInfo', data)
      } catch (err) {
      }
    }
  },
  modules: {
    main,
    sideMenu,
    simpleMenu,
    topMenu,
  }
})

export function useStore() {
  return store
}

export default store
