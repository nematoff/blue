const state = () => {
  return {
    menu: [
      {
        icon: 'BoxIcon',
        pageName: 'simple-menu-menu-layout',
        title: 'Menu Layout',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Side Menu',
            ignore: true
          },
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Simple Menu',
            ignore: true
          },
          {
            icon: '',
            pageName: 'side-menu-dashboard',
            title: 'Top Menu',
            ignore: true
          }
        ]
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-example',
        title: 'Example'
      },

      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-locations',
        title: 'Locations'
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-shop-cabinet',
        title: 'Shop cabinet'
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-shop',
        title: 'Shop'
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-home',
        title: 'Home'
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-price-list',
        title: 'Price'
      },
      {
        icon: 'InboxIcon',
        pageName: 'simple-menu-black-list-users',
        title: 'Black list user'
      },
      'devider',
      {
        icon: 'LayoutIcon',
        pageName: 'simple-menu-layout',
        title: 'Pages',
        subMenu: [
          {
            icon: '',
            pageName: 'login',
            title: 'Login'
          },
          {
            icon: '',
            pageName: 'register',
            title: 'Register'
          },
          {
            icon: '',
            pageName: 'error-page',
            title: 'Error Page'
          },
          {
            icon: '',
            pageName: 'simple-menu-update-profile',
            title: 'Update profile'
          },
          {
            icon: '',
            pageName: 'simple-menu-change-password',
            title: 'Change Password'
          }
        ]
      },
      'devider'
    ]
  }
}

// getters
const getters = {
  menu: state => state.menu
}

// actions
const actions = {}

// mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
