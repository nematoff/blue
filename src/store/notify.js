const state = () => {
  return {
    notify: {}
  }
}

const getters = {
  notify: state => state.notify
}

const mutations = {
  writeNotifyText: (state, payload) => {
    state.notify = payload
  }
}

const actions = {
  showNotify({ commit }, payload) {
    commit('writeNotifyText', payload)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
