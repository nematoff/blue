const state = () => {
  return {
    menu: [
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-dashboard',
        title: 'dashboard',
        checkUser: 'all'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-scoring',
        title: 'scoring',
        checkUser: 'all'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-black-list-users',
        title: 'blacklist',
        checkUser: 'admin'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-shop',
        title: 'branches',
        checkUser: 'admin'
      },
      // {
      //   icon: 'InboxIcon',
      //   pageName: 'side-menu-home',
      //   title: 'search',
      //   checkUser: 'shop'
      // },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-shop-cabinet',
        title: 'blacklist',
        checkUser: 'shop'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-report',
        title: 'report',
        checkUser: 'all'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-transactions',
        title: 'transactions',
        checkUser: 'all'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-locations',
        title: 'regions',
        checkUser: 'admin'
      },
      {
        icon: 'InboxIcon',
        pageName: 'side-menu-price-list',
        title: 'price',
        checkUser: 'admin'
      },

      // {
      //   icon: 'LayoutIcon',
      //   pageName: 'side-menu-layout',
      //   title: 'Pages',
      //   subMenu: [
      //     {
      //       icon: '',
      //       pageName: 'login',
      //       title: 'Login'
      //     },
      //     {
      //       icon: '',
      //       pageName: 'register',
      //       title: 'Register'
      //     },
      //     {
      //       icon: '',
      //       pageName: 'error-page',
      //       title: 'Error Page'
      //     },
      //     {
      //       icon: '',
      //       pageName: 'side-menu-update-profile',
      //       title: 'Update profile'
      //     },
      //     {
      //       icon: '',
      //       pageName: 'side-menu-change-password',
      //       title: 'Change Password'
      //     }
      //   ]
      // },
      // 'devider'
    ]
  }
}

// getters
const getters = {
  menu: state => state.menu
}

// actions
const actions = {}

// mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
